# Tic-Toc-Toe

Implementazione di un sistema di API Rest per il gioco Tic-Tac-Toe (tris)

### Autore: Roberto Parrella

## Requisiti

Docker

## Installazione

Da terminale scaricare

```bash
git clone https://robertowebdeveloper@bitbucket.org/robertowebdeveloper/tic-tac-toe.git tic-tac-toe
cd tic-tac-toe/docker
docker-compose up
```

## Prove

Nella cartella "testcase" è contenuto il file testcase.http, che può essere eseguito tramite IDE PhpStorm, oppure si può replicare i casi tramite interfaccia cURL o applicativi come Postman

## Endpoints
Tutti gli endpoints richiedono un'autenticazione di tipo Basic:

```bash
Basic tictactoe a8b5d1a28705$443a$Aff0$E74582fd055a
```

### Creazione nuovo gioco
```
[POST] http://localhost:81/api/v1/game
```
Ritorno l'uuid di riferimento, utile per le chiamate successive.

### Mossa
```
[PUT] http://localhost:81/api/v1/game/{uuid}/{playerNumber}
body in JSON: 
{
  "x" : 0,
  "y" : 0
}
```

### Richiesta stato della partita
```
[GET] http://localhost:81/api/v1/game/{uuid}
```