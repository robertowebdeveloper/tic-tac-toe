<?php

namespace App\Service;

use App\Entity\Game;
use App\Repository\GameRepository;
use App\Response\GameResponse;
use Symfony\Component\Uid\Uuid;

class GameService
{
    /** @var GameRepository */
    private $gameRepository;

    /**
     * @param GameRepository $gameRepository
     */
    public function __construct(GameRepository $gameRepository)
    {
        $this->gameRepository = $gameRepository;
    }

    /**
     * @return Game
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function newGame(): Game
    {
        $game = new Game();
        do {
            $uuid = (Uuid::v4())->toRfc4122();
            $flag = $this->gameRepository->existsUuid($uuid);
        } while ($flag);
        $game->setUuid($uuid);
        $this->gameRepository->save($game);
        return $game;
    }

    /**
     * @param string $uuid
     * @param int $player
     * @param int $x
     * @param int $y
     * @return GameResponse
     * @throws \Exception
     */
    public function playGame(string $uuid, int $player, int $x, int $y): GameResponse
    {
        $game = $this->gameRepository->findOneBy([
            'uuid' => $uuid,
        ]);
        if ($game === null) {
            throw new \Exception('Game not found');
        } else if ($game->isCompleted()) {
            throw new \Exception('Game is completed');
        } else if ($game->getLastPlayerToMove() === $player) {
            throw new \Exception(sprintf("It's not player #%s turn", $player));
        }

        $grid = $game->getGrid();
        if ($grid[$x][$y] === null) {
            $grid[$x][$y] = Game::PLAYER_BADGES[$player];
        } else {
            throw new \Exception(sprintf('Position %s,%s is not empty', $x, $y));
        }
        $game->setGrid($grid);
        $game->setLastPlayerToMove($player);
        $game = $this->gameRepository->save($game);
        return new GameResponse($game);
    }

    /**
     * @param string $uuid
     * @return Game|null
     */
    public function getGameFromUuid(string $uuid): ?Game
    {
        return $this->gameRepository->findOneBy([
            'uuid' => $uuid,
        ]);
    }
}
