<?php

namespace App\Response;

use App\Entity\Game;

class GameResponse
{
    /**
     * @var string|null
     */
    private $uuid;

    /** @var array|null */
    private $grid;

    /** @var bool */
    private $isCompleted;

    /** @var bool  */
    private $hasWinner;

    /** @var int|null  */
    private $playerWinner;

    /** @var bool */
    private $checkMate;

    /** @var bool */
    private $checkMateGaming;

    /**
     * @param Game $game
     * @throws \Exception
     */
    public function __construct(Game $game, int $player = null)
    {
        $this->uuid = $game->getUuid();
        $this->grid = $game->getGrid();
        $this->isCompleted = $game->isCompleted();
        $this->hasWinner = $game->hasWinner();
        $this->playerWinner = $game->getWinner();
        $this->checkMate = null === $player ? null : $game->getCheckMate($player);
        $this->checkMateGaming = null === $player ? null : $game->getCheckMateGaming($player);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'uuid' => $this->uuid,
            'grid' => $this->grid,
            'isCompleted' => $this->isCompleted,
            'hasWinner' => $this->hasWinner,
            'playerWinner' => $this->playerWinner,
            'checkMate' => $this->checkMate,
            'checkMateGaming' => $this->checkMateGaming,
        ];
    }
}
