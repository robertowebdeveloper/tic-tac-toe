<?php

namespace App\Controller;

use App\Response\GameResponse;
use App\Service\GameService;
use App\Validator\Position;
use App\Validator\PositionValidator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/v1')]
class ApiController extends AbstractApiController
{
    /** @var GameService */
    private $gameService;

    public function __construct(GameService $gameService)
    {
        $this->gameService = $gameService;
    }

    #[Route('/game', name: 'start-game', methods: ['post'])]
    public function startGame(): JsonResponse
    {
        try {
            $game = $this->gameService->newGame();
            return new JsonResponse([
                'uuid' => $game->getUuid(),
            ], JsonResponse::HTTP_CREATED);
        } catch (\Exception $e) {
            return $this->ErrorResponse($e->getMessage());
        }
    }

    #[Route('/game/{uuid}/{player}', name: 'play-game', methods: ['PUT'])]
    public function playGame(string $uuid, string $player, Request $request, ValidatorInterface $validator): JsonResponse
    {
        $data = json_decode($request->getContent(), false);
        $x = $data->x ?? null;
        $y = $data->y ?? null;

        $positionsConstraints = [
            new GreaterThanOrEqual(0, message: "X or Y should be greater than or equal to 0."),
            new LessThanOrEqual(2, message: "X or Y should be less than or equal to 2."),
            new NotNull(null, "X or Y should not be null."),
        ];

        $violations = $validator->validate($x, $positionsConstraints);
        if ($violations->count() > 0) {
            return $this->ErrorViolationsResponse($violations);
        }
        $violations = $validator->validate($y, $positionsConstraints);
        if ($violations->count() > 0) {
            return $this->ErrorViolationsResponse($violations);
        }
        $violations = $validator->validate($player, [
            new GreaterThanOrEqual(1, message: "Player should be 1 or 2"),
            new LessThanOrEqual(2, message: "Player should be 1 or 2"),
        ]);
        if ($violations->count() > 0) {
            return $this->ErrorViolationsResponse($violations);
        }

        try {
            $response = $this->gameService->playGame($uuid, (int)$player, (int)$x, (int)$y);
            return new JsonResponse($response->toArray());
        } catch (\Exception $e) {
            return $this->ErrorResponse($e->getMessage());
        }
    }

    #[Route('/game/{uuid}/{player}', name: 'get-game', methods: ['get'])]
    public function getGame(string $uuid, string $player = null): JsonResponse
    {
        $player = $player !== null ? (int)$player : null;
        $game = $this->gameService->getGameFromUuid($uuid);
        if ($game === null) {
            return $this->ErrorResponse(sprintf('Game %s not found', $uuid), JsonResponse::HTTP_BAD_REQUEST);
        }
        $response = new GameResponse($game, $player);
        return new JsonResponse($response->toArray());
    }
}
