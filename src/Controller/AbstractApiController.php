<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\ConstraintViolationListInterface;

abstract class AbstractApiController extends AbstractController
{
    /**
     * @param string $message
     * @param int $code
     * @return JsonResponse
     */
    protected function ErrorResponse(string $message, int $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR): JsonResponse
    {
        return new JsonResponse([
            'message' => $message,
        ], $code);
    }

    /**
     * @param ConstraintViolationListInterface $constraintViolationList
     * @return JsonResponse
     */
    protected function ErrorViolationsResponse(ConstraintViolationListInterface $constraintViolationList): JsonResponse
    {
        $arrayMessage = [];
        foreach ($constraintViolationList as $violation) {
            $arrayMessage[] = $violation->getMessage();
        }
        return $this->ErrorResponse(implode(" - ", $arrayMessage));
    }
}
