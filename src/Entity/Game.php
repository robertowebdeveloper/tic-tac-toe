<?php

namespace App\Entity;

use App\Repository\GameRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GameRepository::class)
 */
class Game
{
    const PLAYER_BADGES = [
        1 => 'X',
        2 => 'O'
    ];

    const GRID_PLACEHOLDER = '-';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=36, unique=true)
     */
    private $uuid;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $grid = [];

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $lastPlayerToMove;

    public function __construct()
    {
        $this->createdAt = new DateTimeImmutable('now');
        $this->setGrid([
            0 => [null, null, null],
            1 => [null, null, null],
            2 => [null, null, null],
        ]);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     * @return $this
     */
    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param DateTimeImmutable|null $createdAt
     * @return $this
     */
    public function setCreatedAt(?DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getGrid(): ?array
    {
        return $this->grid;
    }

    /**
     * @param array|null $grid
     * @return $this
     */
    public function setGrid(?array $grid): self
    {
        $this->grid = $grid;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCompleted(): bool
    {
        if ($this->hasWinner()) {
            return true;
        }

        $n = 0;
        foreach ($this->getGrid() as $row) {
            foreach ($row as $col) {
                if ($col !== null) {
                    $n++;
                }
            }
        }
        return ($n === 9);
    }

    /**
     * @return bool
     */
    public function hasWinner(): bool
    {
        return ($this->getWinner() !== null);
    }

    /**
     * @param int $player
     * @return bool
     */
    public function getCheckMate(int $player): bool
    {
        return $this->computeGameStatus($player)['checkMate'];
    }

    /**
     * @param int $player
     * @return bool
     */
    public function getCheckMateGaming(int $player): bool
    {
        return $this->computeGameStatus($player)['checkMateGaming'];
    }

    /**
     * @return int|null
     * @throws \Exception
     */
    public function getWinner(): ?int
    {
        return $this->computeGameStatus()['playerWin'];
    }

    /**
     * @param int|null $player
     * @return array
     * @throws \Exception
     */
    private function computeGameStatus(int $player = null): array
    {
        $response = [
            'playerWin' => null,
            'checkMate' => false,
            'checkMateGaming' => false
        ];
        $nCheckMate = 0;

        $grid = $this->getGrid();
        $gridForCheckMate = $this->getGrid();
        if ($grid === null) {
            throw new \Exception('Grid not initialized');
        }

        // Verify columns
        for ($i = 0; $i < 3; $i++) {
            for ($v = '', $vCheckMate = '', $j = 0; $j < 3; $j++) {
                $v .= $grid[$i][$j] ?? '';
                if (!empty($gridForCheckMate[$i][$j])) {
                    $vCheckMate .= $gridForCheckMate[$i][$j];
                } else {
                    $gridForCheckMate[$i][$j] = self::GRID_PLACEHOLDER;
                }
            }
            if ($playerWin = $this->hasWinnerPlayer($v)) {
                $response['playerWin'] = $playerWin;
            }
            if ($this->hasCheckMate($player, $vCheckMate)) {
                $nCheckMate++;
                $gridForCheckMate = $this->persistPlaceholderOnCheckMateGrid($gridForCheckMate, $player);
            } else {
                $gridForCheckMate = $this->clearPlaceholderOnCheckMateGrid($gridForCheckMate);
            }
        }

        // Verify rows
        for ($i = 0; $i < 3; $i++) {
            for ($v = '', $vCheckMate = '', $j = 0; $j < 3; $j++) {
                $v .= $grid[$j][$i] ?? '';
                if (!empty($gridForCheckMate[$j][$i])) {
                    $vCheckMate .= $gridForCheckMate[$j][$i];
                } else {
                    $gridForCheckMate[$j][$i] = self::GRID_PLACEHOLDER;
                }
            }
            if ($playerWin = $this->hasWinnerPlayer($v)) {
                $response['playerWin'] = $playerWin;
            }

            if ($this->hasCheckMate($player, $vCheckMate)) {
                $nCheckMate++;
                $gridForCheckMate = $this->persistPlaceholderOnCheckMateGrid($gridForCheckMate, $player);
            } else {
                $gridForCheckMate = $this->clearPlaceholderOnCheckMateGrid($gridForCheckMate);
            }
        }

        // Verify diagonals
        for ($v = '', $vCheckMate = '', $i = 0; $i < 3; $i++) {
            $v .= $grid[$i][$i] ?? '';
            if (!empty($gridForCheckMate[$i][$i])) {
                $vCheckMate .= $gridForCheckMate[$i][$i];
            } else {
                $gridForCheckMate[$i][$i] = self::GRID_PLACEHOLDER;
            }
        }
        if ($playerWin = $this->hasWinnerPlayer($v)) {
            $response['playerWin'] = $playerWin;
        }
        if ($this->hasCheckMate($player, $vCheckMate)) {
            $nCheckMate++;
            $gridForCheckMate = $this->persistPlaceholderOnCheckMateGrid($gridForCheckMate, $player);
        } else {
            $gridForCheckMate = $this->clearPlaceholderOnCheckMateGrid($gridForCheckMate);
        }

        for ($v = '', $vCheckMate = '', $i = 0, $j = 2; $i < 3; $i++, $j--) {
            $v .= $grid[$i][$j] ?? '';
            if (!empty($gridForCheckMate[$i][$j])) {
                $vCheckMate .= $gridForCheckMate[$i][$j];
            } else {
                $gridForCheckMate[$i][$j] = self::GRID_PLACEHOLDER;
            }
        }
        if ($playerWin = $this->hasWinnerPlayer($v)) {
            $response['playerWin'] = $playerWin;
        }
        if ($this->hasCheckMate($player, $vCheckMate)) {
            $nCheckMate++;
            $gridForCheckMate = $this->persistPlaceholderOnCheckMateGrid($gridForCheckMate, $player);
        } else {
            $gridForCheckMate = $this->clearPlaceholderOnCheckMateGrid($gridForCheckMate);
        }

        $response['checkMate'] = ($nCheckMate > 0);
        $response['checkMateGaming'] = ($nCheckMate > 1);

        return $response;
    }

    /**
     * @param array $gridForCheckMate
     * @param int $player
     * @return array
     */
    private function persistPlaceholderOnCheckMateGrid(array $gridForCheckMate, int $player): array
    {
        return $this->setPlaceholderOnCheckMateGrid($gridForCheckMate, self::PLAYER_BADGES[$player]);
    }

    /**
     * @param array $gridForCheckMate
     * @return array
     */
    private function clearPlaceholderOnCheckMateGrid(array $gridForCheckMate): array
    {
        return $this->setPlaceholderOnCheckMateGrid($gridForCheckMate);
    }

    /**
     * @param array $gridForCheckMate
     * @param string $value
     * @return array
     */
    private function setPlaceholderOnCheckMateGrid(array $gridForCheckMate, string $value = ''): array
    {
        foreach ($gridForCheckMate as $i => $row) {
            foreach ($gridForCheckMate as $j => $col) {
                if ($gridForCheckMate[$i][$j] === '-') {
                    $gridForCheckMate[$i][$j] = $value;
                }
            }
        }
        return $gridForCheckMate;
    }

    /**
     * @param string $value
     * @return int|null
     */
    private function hasWinnerPlayer(string $value): ?int
    {
        $playersWinBadge = [];
        foreach (self::PLAYER_BADGES as $playerKey => $badge) {
            for ($playersWinBadge[$playerKey] = '', $i = 0; $i < 3; $i++) {
                $playersWinBadge[$playerKey] .= $badge;
            }
        }

        foreach ($playersWinBadge as $playerKey => $badgeWin) {
            if ($value === $badgeWin) {
                return $playerKey;
            }
        }
        return null;
    }

    /**
     * @param int|null $player |null
     * @param string $value
     * @return bool
     */
    private function hasCheckMate(?int $player, string $value): bool
    {
        if ($player === null) {
            return false;
        }
        return ($value === self::PLAYER_BADGES[$player] . self::PLAYER_BADGES[$player]);
    }

    /**
     * @return int|null
     */
    public function getLastPlayerToMove(): ?int
    {
        return $this->lastPlayerToMove;
    }

    /**
     * @param int|null $lastPlayerToMove
     * @return $this
     * @throws \Exception
     */
    public function setLastPlayerToMove(?int $lastPlayerToMove): self
    {
        if (!array_key_exists($lastPlayerToMove, self::PLAYER_BADGES)) {
            throw new \Exception(sprintf("Player to move value is not valid. Allowed: %s", implode(", ", array_keys(self::PLAYER_BADGES))));
        }
        $this->lastPlayerToMove = $lastPlayerToMove;

        return $this;
    }
}
